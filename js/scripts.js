var activateform = $('.takeaction__button');
var hiddenform = $('.hiddenform');
var closeForm = $('.close');
var $header = $("header");
var arrowup = $('.arrowup');
var slides = document.querySelectorAll('#slides .slide');
var hamburgerIcon = $('.hamburger');
var menusectioncontent = $('.menusection');
var hamburgermenu = $('.menutriger');
var formname = $('.formintem__name');
var formmail = $('.formintem__email');
var fromphone = $('.formintem__phone');
var formtextarea = $('.formintem__textaera');
var hiddentext = $('.formintem__texthidden');
var emailhidentext = $('.formintem__mail');
var phonehidentext = $('.formintem__phonetext');


$(document).ready(function () {
 $clone = $header.before($header.clone().addClass("clone"));
 $(window).on("scroll", function  () {
    var fromTop = $("body").scrollTop();
    $('body').toggleClass("down", (fromTop > 100));
    $('.active__menu').css({top:0});
  });
  activateform.on('click', function()    {
    hiddenform.slideDown();
    return false;
  });
  closeForm.on('click', function() {
    hiddenform.slideUp();
  });
  arrowup.on('click', function() {
        $("html, body").animate({ scrollTop: 0 }, "slow");
        return false;
    });
     $(window).scroll(function () {
        if ($(this).scrollTop() > 100) {
            arrowup.fadeIn();
        } else {
            arrowup.fadeOut();
        }
    });
  /*hamburgerIcon.on('click', function  () {
    menusectioncontent.toggleClass('menusectionShow');
  });*/

  hamburgermenu.on('click' , function(){
      $('.stripe__list').toggleClass('active__menu');
      $('.mainmenusocial').toggleClass('active__menu');
  });  
    
  $('.owl-carousel').owlCarousel({
    loop: true,
    margin: 0,
    nav: false,
    responsive:{
        0: {
            items: 1
        },
        600: {
            items: 1
        },
        1000: {
            items: 1
        }
    }
})
});

function validateEmail(formmail) {
  'use strict'
  var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
  return emailReg.test( formmail );
}

function validate() {
    if( (formname.val().length === 0) ){
        formname.addClass('error');
        hiddentext.show();
    } else {
        formname.removeClass('error');
         hiddentext.hide();
    }
    if( (formmail.val().length === 0) ){
        formmail.addClass('error');
        !validateEmail(formmail);
        emailhidentext.show();
    }else{
        formmail.removeClass('error');
        emailhidentext.hide();
    }
    if( (fromphone.val().length === 0) ){
        fromphone.addClass('error');
        phonehidentext.show();
    } else {
        fromphone.removeClass('error');
         phonehidentext.hide();
    }
    if( (formtextarea.val().length === 0) ){
        formtextarea.addClass('error');
    } else {
        formtextarea.removeClass('error');
    }
}

